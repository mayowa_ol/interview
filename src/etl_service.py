import pandas as pd


class EtlService(object):

    @classmethod
    def extract(cls, file_path):
        """
        This extracts data from the source CSV path and converts it to a data frame

        :param file_path: Full path of the source file location
        :return: A Dataframe of the extracted CSV file
        """
        try:
            file = pd.read_csv(file_path)
            df = pd.DataFrame(file)
        except FileNotFoundError:
            raise Exception('File path is not valid')
        return df

    @classmethod
    def rename_columns(cls, file_path, columns_dict):
        """
        This renames specified columns in a particular file

        :param file_path: Full path of the source file location
        :param columns_dict: A dictionary that contains the columns' current name as key and new name as value,
                             an example is  {'user': 'user_name', 'phone': 'phone_number'
        :return: A Dataframe with the specified columns renamed
        """
        file = cls.extract(file_path=file_path)
        renamed = file.rename(columns=columns_dict, errors='raise')
        return renamed

    @classmethod
    def convert_file_to_dict(cls, file_path):
        """
        Converts a CSV file to a dictionary, and sets id as key and the corresponding row as value

        :param file_path: Full path of the source file location
        :return: Returns a dictionary representation of the source CSV file
        """
        try:
            data = {}
            for key, value in pd.read_csv(file_path).iterrows():
                file_dict = {value.id: value}
                data.update(file_dict)
        except FileNotFoundError:
            raise Exception('File not found, please provide full path of file')
        return data

    @classmethod
    def load_data_to_csv(cls, resource, file_name):
        """
        Loads data from a dataframe to a CSV file

        :param resource: Name of the dataframe to be loaded into a CSV file
        :param file_name: Name to be given to the file that would be loaded
        :return: Saved CSV file
        """
        return resource.to_csv(file_name, index=False)
