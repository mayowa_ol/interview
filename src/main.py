import pandas as pd
from src.etl_service import EtlService

def main():
    """"Process and Merge the source data files"""

    # rename hospital_visits_columns
    hospital_visit = EtlService.rename_columns(file_path='../files/hospital_visits.csv', columns_dict=
   {'id': 'hospital_visit_id', 'created_at': 'hospital_visit_created_at', 'type': 'hospital_visit_type'})

   # rename patients table columns
    patients = EtlService.rename_columns(file_path='../files/patients.csv',
                                        columns_dict={'id': 'patient_id', 'name': 'patient_name',
                                                                            'created_at': 'patient_created_at',
                                                                            'sex': 'patient_sex'})

    # rename doctor columns
    doctors = EtlService.rename_columns(file_path='../files/doctors.csv', columns_dict={'id': 'doctor_id',
                                                                                    'name': 'doctor_name',
                                                                                    'created_at': 'doctor_created_at'})
   # merge renamed columns into a csv file
    merge_data = pd.merge(left = hospital_visit, right=doctors,
                         left_on='doctor_id', right_on='doctor_id').merge(patients,
                         left_on='patient_id', right_on='patient_id')

   # load merged data result to csv
    load = EtlService.load_data_to_csv(resource=merge_data, file_name='../result/hospital_patients.csv')
    return load


if __name__ == '__main__':
    main()
