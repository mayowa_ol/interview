-- Number of hospital visits per day over the period
SELECT hospital_visit_created_at AS visit_date,
       COUNT(hospital_visit_id) AS hospital_visits
FROM hospital_patients
GROUP BY hospital_visit_created_at;


-- Number of patients attended to per doctor per month showing their names
SELECT DATE_FORMAT(hospital_visit_created_at, '%Y-%m') AS visit_month,
       doctor_name,
       COUNT(DISTINCT patient_id) AS total_patients
FROM hospital_patients
GROUP BY visit_month, doctor_name
ORDER BY visit_month, doctor_name;


-- The ratio of female to male patient visits per month
SELECT
   DATE_FORMAT(hospital_visit_created_at, '%Y-%m') AS visit_month,
  SUM(IF(patient_sex = 'female', 1, 0)) / SUM(IF(patient_sex = 'male', 1, 0)) AS female_to_male_ratio
FROM hospital_patients
GROUP BY visit_month
ORDER BY visit_month;
